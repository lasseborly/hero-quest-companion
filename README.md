#Hero Quest Companion

######A companion app meant for being used in conjunction with the HeroQuest board game.

The app allows the user to create, save and load characters. Characters can also be saved onto NFC tags and loaded off of them.

##Technology

* ###[Apache Commons Codec](https://commons.apache.org/proper/commons-codec/)
     
    Used as a way to simplify the transport and storing of characters on the NFC tags. The idea of saving the whole character object instead of a referance was not that great of an idea in the perspective of cheap small tags but the novelty of having a NFC attached to an action figure, scanning it and asseing the characters stats eluded me.    
***