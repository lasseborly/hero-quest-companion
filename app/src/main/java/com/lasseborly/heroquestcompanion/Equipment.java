package com.lasseborly.heroquestcompanion;

import java.io.Serializable;

/**
 * Created by ALILBO on 21-04-2015.
 */
public class Equipment implements Serializable {

    private static final long serialVersionUID = -6092623100039875002L;
    private int id;
    private String name;
    private String description;
    private int cost;

    public Equipment() {
    }

    public Equipment(int id, String name, String description, int cost) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", cost=" + cost +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
