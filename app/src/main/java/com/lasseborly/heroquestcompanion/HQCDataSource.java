package com.lasseborly.heroquestcompanion;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ALILBO on 24-04-2015.
 */
public class HQCDataSource {



    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;

    private static final String[] allCharacterColumns = {
            HQCDBOpenHelper.COLUMN_ID,
            HQCDBOpenHelper.COLUMN_NAME,
            HQCDBOpenHelper.COLUMN_MIND,
            HQCDBOpenHelper.COLUMN_BODY,
            HQCDBOpenHelper.COLUMN_ATTACK,
            HQCDBOpenHelper.COLUMN_DEFEND,
            HQCDBOpenHelper.COLUMN_MOVE,
            HQCDBOpenHelper.COLUMN_CHARACTER_FOREIGN_ID};


    public HQCDataSource(Context context) {

        dbhelper = new HQCDBOpenHelper(context);

    }

    public void open() {
        database = dbhelper.getWritableDatabase();
    }

    public void close() {
        dbhelper.close();
    }

    public Character createCharacter(Character character) {

        ContentValues values = new ContentValues();
        values.put(HQCDBOpenHelper.COLUMN_NAME, character.getName());
        values.put(HQCDBOpenHelper.COLUMN_MIND, character.getMind());
        values.put(HQCDBOpenHelper.COLUMN_BODY, character.getBody());
        values.put(HQCDBOpenHelper.COLUMN_ATTACK, character.getAttack());
        values.put(HQCDBOpenHelper.COLUMN_DEFEND, character.getDefend());
        values.put(HQCDBOpenHelper.COLUMN_MOVE, character.getMove());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_FOREIGN_ID, character.getCharacterClass().getId());
        long insertid = database.insert(HQCDBOpenHelper.TABLE_CHARACTERS, null, values);

        character.setId(insertid);

        return character;
    }

    public void deleteCharacter(Character character) {

        database.delete(HQCDBOpenHelper.TABLE_CHARACTERS," " + HQCDBOpenHelper.COLUMN_ID + " = " + character.getId(), null);

        Log.d("LOGTAG", "Character Deleted");
    }

    public Character updateCharacter(Character character) {

        ContentValues values = new ContentValues();
        values.put(HQCDBOpenHelper.COLUMN_MIND, character.getMind());
        values.put(HQCDBOpenHelper.COLUMN_BODY, character.getBody());
        values.put(HQCDBOpenHelper.COLUMN_ATTACK, character.getAttack());
        values.put(HQCDBOpenHelper.COLUMN_DEFEND, character.getDefend());
        values.put(HQCDBOpenHelper.COLUMN_MOVE, character.getMove());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_FOREIGN_ID, character.getCharacterClass().getId());

        String selection = HQCDBOpenHelper.COLUMN_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(character.getId()) };

        database.update(
                HQCDBOpenHelper.TABLE_CHARACTERS,
                values,
                selection,
                selectionArgs);

        return character;
    }

    public CharacterClass createCharacterClass(CharacterClass characterClass) {

        ContentValues values = new ContentValues();
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_NAME, characterClass.getClassName());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_DESCRIPTION, characterClass.getClassDescription());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_MIND, characterClass.getClassMind());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_BODY, characterClass.getClassBody());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_ATTACK, characterClass.getClassAttack());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_DEFEND, characterClass.getClassDefend());
        values.put(HQCDBOpenHelper.COLUMN_CHARACTER_MOVE, characterClass.getClassMove());

        long insertid = database.insert(HQCDBOpenHelper.TABLE_CHARACTERSCLASSES, null, values);

        characterClass.setId(insertid);

        return characterClass;
    }

    public List<Character> findAllCharacters() {

        List<Character> characters = new ArrayList<Character>();

        Cursor cursor = database.query(HQCDBOpenHelper.TABLE_CHARACTERS, allCharacterColumns,
                null, null, null, null, null);

        if(cursor.getCount() > 0) {

            while(cursor.moveToNext()) {
                Character character = new Character();
                character.setId(cursor.getLong(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_ID)));
                character.setName(cursor.getString(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_NAME)));
                character.setMind(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_MIND)));
                character.setBody(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_BODY)));
                character.setAttack(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_ATTACK)));
                character.setDefend(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_DEFEND)));
                character.setMove(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_MOVE)));
                character.setCharacterClass(findCharacterClass(cursor.getLong(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_FOREIGN_ID))));
                Log.d("CHARACTER", character.toString());
                characters.add(character);
            }

        }
        return characters;
    }

    public CharacterClass findCharacterClass(long id) {

        CharacterClass characterClass = new CharacterClass();

        Cursor cursor = null;

        try{

            cursor = database.rawQuery("SELECT * FROM "+ HQCDBOpenHelper.TABLE_CHARACTERSCLASSES +" WHERE " + HQCDBOpenHelper.COLUMN_CHARACTER_PRIMARY_ID + "=" + id, null);

            if(cursor.getCount() > 0) {

                cursor.moveToFirst();
                characterClass.setId(cursor.getLong(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_PRIMARY_ID)));
                characterClass.setClassName(cursor.getString(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_NAME)));
                characterClass.setClassDescription(cursor.getString(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_DESCRIPTION)));
                characterClass.setClassMind(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_MIND)));
                characterClass.setClassBody(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_BODY)));
                characterClass.setClassAttack(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_ATTACK)));
                characterClass.setClassDefend(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_DEFEND)));
                characterClass.setClassMove(cursor.getInt(cursor.getColumnIndex(HQCDBOpenHelper.COLUMN_CHARACTER_MOVE)));

            }

            return characterClass;
        }finally {

            cursor.close();
        }

    }





}
