package com.lasseborly.heroquestcompanion;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ALILBO on 24-04-2015.
 */
public class HQCDBOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "hqc.db";
    private static final int DATABASE_VERSION = 16;

    public static final String TABLE_CHARACTERS = "characters";
    public static final String COLUMN_ID        = "id";
    public static final String COLUMN_NAME      = "name";
    public static final String COLUMN_MIND      = "mind";
    public static final String COLUMN_BODY      = "body";
    public static final String COLUMN_ATTACK    = "attack";
    public static final String COLUMN_DEFEND    = "defend";
    public static final String COLUMN_MOVE      = "move";
    public static final String COLUMN_CHARACTER_FOREIGN_ID        = "characterId";


    public static final String TABLE_CHARACTERSCLASSES = "characterClasses";
    public static final String COLUMN_CHARACTER_PRIMARY_ID        = "id";
    public static final String COLUMN_CHARACTER_NAME      = "characterName";
    public static final String COLUMN_CHARACTER_DESCRIPTION      = "characterDescription";
    public static final String COLUMN_CHARACTER_MIND      = "characterMind";
    public static final String COLUMN_CHARACTER_BODY      = "characterBody";
    public static final String COLUMN_CHARACTER_ATTACK    = "characterAttack";
    public static final String COLUMN_CHARACTER_DEFEND    = "characterDefend";
    public static final String COLUMN_CHARACTER_MOVE      = "characterMove";

    private static final String TABLE_CHARACTERS_CREATE = "CREATE TABLE " + TABLE_CHARACTERS +
            " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NAME + " TEXT, " +
            COLUMN_MIND + " INTEGER, " +
            COLUMN_BODY + " INTEGER, " +
            COLUMN_ATTACK + " INTEGER, " +
            COLUMN_DEFEND + " INTEGER, " +
            COLUMN_MOVE + " INTEGER, " +
            COLUMN_CHARACTER_FOREIGN_ID + " INTEGER " +
            ")";

    private static final String TABLE_CHARACTERCLASSES_CREATE = "CREATE TABLE " + TABLE_CHARACTERSCLASSES + " (" +
            COLUMN_CHARACTER_PRIMARY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_CHARACTER_NAME + " TEXT, " +
            COLUMN_CHARACTER_DESCRIPTION + " TEXT, " +
            COLUMN_CHARACTER_MIND + " INTEGER, " +
            COLUMN_CHARACTER_BODY + " INTEGER, " +
            COLUMN_CHARACTER_ATTACK + " INTEGER, " +
            COLUMN_CHARACTER_DEFEND + " INTEGER, " +
            COLUMN_CHARACTER_MOVE + " INTEGER " +
            ")"
            ;

    private static final String CREATE_WIZARD = "INSERT INTO " + TABLE_CHARACTERSCLASSES + " (" +
            COLUMN_CHARACTER_NAME + ", " +
            COLUMN_CHARACTER_DESCRIPTION + ", " +
            COLUMN_CHARACTER_MIND + ", " +
            COLUMN_CHARACTER_BODY + ", " +
            COLUMN_CHARACTER_ATTACK + ", " +
            COLUMN_CHARACTER_DEFEND + ", " +
            COLUMN_CHARACTER_MOVE + ") " +
            "VALUES ('Wizard', 'You are the Wizard, You have many spells that can aid you. " +
            "However, in combat you are weak. So use your spells well and avoid combat.', 6, 4, 1, 2, 2);";

    private static final String CREATE_BARBARIAN = "INSERT INTO " + TABLE_CHARACTERSCLASSES + " (" +
            COLUMN_CHARACTER_NAME + ", " +
            COLUMN_CHARACTER_DESCRIPTION + ", " +
            COLUMN_CHARACTER_MIND + ", " +
            COLUMN_CHARACTER_BODY + ", " +
            COLUMN_CHARACTER_ATTACK + ", " +
            COLUMN_CHARACTER_DEFEND + ", " +
            COLUMN_CHARACTER_MOVE + ") " +
            "VALUES ('Barbarian', 'You are the Barbarian, the greatest warrior of all. " +
            "But beware of magic for your sword is no defence against it.', 2, 8, 3, 2, 2);";

    private static final String CREATE_ELF = "INSERT INTO " + TABLE_CHARACTERSCLASSES + " (" +
            COLUMN_CHARACTER_NAME + ", " +
            COLUMN_CHARACTER_DESCRIPTION + ", " +
            COLUMN_CHARACTER_MIND + ", " +
            COLUMN_CHARACTER_BODY + ", " +
            COLUMN_CHARACTER_ATTACK + ", " +
            COLUMN_CHARACTER_DEFEND + ", " +
            COLUMN_CHARACTER_MOVE + ") " +
            "VALUES ('Elf', 'You are the Elf. A master of both magic and the sword. " +
            "You must use both well if you are to triumph.', 4, 6, 2, 2, 2);";

    private static final String CREATE_DWARF = "INSERT INTO " + TABLE_CHARACTERSCLASSES + " (" +
            COLUMN_CHARACTER_NAME + ", " +
            COLUMN_CHARACTER_DESCRIPTION + ", " +
            COLUMN_CHARACTER_MIND + ", " +
            COLUMN_CHARACTER_BODY + ", " +
            COLUMN_CHARACTER_ATTACK + ", " +
            COLUMN_CHARACTER_DEFEND + ", " +
            COLUMN_CHARACTER_MOVE + ") " +
            "VALUES ('Dwarf', 'You are the Dwarf. You are a good warrior and can always disarm traps that you find. " +
            "You may remove any visible trap in the same room or passage.', 3, 7, 2, 2, 2);";


    public HQCDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CHARACTERS_CREATE);
        db.execSQL(TABLE_CHARACTERCLASSES_CREATE);
        db.execSQL(CREATE_BARBARIAN);
        db.execSQL(CREATE_DWARF);
        db.execSQL(CREATE_ELF);
        db.execSQL(CREATE_WIZARD);

        Log.d("LOGTAG", "Table has been created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHARACTERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHARACTERSCLASSES);
        onCreate(db);
    }
}
