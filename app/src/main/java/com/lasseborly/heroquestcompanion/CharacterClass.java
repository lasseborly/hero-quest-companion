package com.lasseborly.heroquestcompanion;

import java.io.Serializable;

/**
 * Created by ALILBO on 21-04-2015.
 */
public class CharacterClass implements Serializable {

    private static final long serialVersionUID = 6064235677114751489L;
    private long id;
    private String className;
    private String classDescription;
    private int classBody;
    private int classMind;
    private int classAttack;
    private int classDefend;
    private int classMove;

    public CharacterClass()  {
    }

    public CharacterClass(int id, String className, String classDescription, int classBody, int classMind, int classAttack, int classDefend, int classMove) {
        this.id = id;
        this.className = className;
        this.classDescription = classDescription;
        this.classBody = classBody;
        this.classMind = classMind;
        this.classAttack = classAttack;
        this.classDefend = classDefend;
        this.classMove = classMove;
    }

    @Override
    public String toString() {
        return "CharacterClass{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", classDescription='" + classDescription + '\'' +
                ", classBody=" + classBody +
                ", classMind=" + classMind +
                ", classAttack=" + classAttack +
                ", classDefend=" + classDefend +
                ", classMove=" + classMove +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public String getClassDescription() {
        return classDescription;
    }

    public void setClassDescription(String classDescription) {
        this.classDescription = classDescription;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getClassBody() {
        return classBody;
    }

    public void setClassBody(int classBody) {
        this.classBody = classBody;
    }

    public int getClassMind() {
        return classMind;
    }

    public void setClassMind(int classMind) {
        this.classMind = classMind;
    }

    public int getClassAttack() {
        return classAttack;
    }

    public void setClassAttack(int classAttack) {
        this.classAttack = classAttack;
    }

    public int getClassDefend() {
        return classDefend;
    }

    public void setClassDefend(int classDefend) {
        this.classDefend = classDefend;
    }

    public int getClassMove() {
        return classMove;
    }

    public void setClassMove(int classMove) {
        this.classMove = classMove;
    }
}
