package com.lasseborly.heroquestcompanion;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;


public class CharacterActivity extends Activity {

    private NumberPicker characterBody;
    private NumberPicker characterMind;
    private NumberPicker characterAttack;
    private NumberPicker characterDefend;

    boolean mWriteMode = false;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mNfcPendingIntent;

    HQCDataSource dataSource;

    private Character character;

    String serializedCharacter;

    Dialog nfcDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        nfcDialog = new Dialog(this);
        nfcDialog.setContentView(R.layout.character_dialog);
        nfcDialog.setTitle("Reading...");

        dataSource = new HQCDataSource(this);

        character = (Character) getIntent().getSerializableExtra(CharacterListActivity.CHARACTER);
        getActionBar().setTitle(character.getName());

        ImageView characterPortrait = (ImageView) findViewById(R.id.characterPortrait);
        TextView characterName = (TextView) findViewById(R.id.characterClass);
        TextView characterDescription = (TextView) findViewById(R.id.characterDescription);

        characterBody = (NumberPicker) findViewById(R.id.characterBody);
        characterMind = (NumberPicker) findViewById(R.id.characterMind);
        characterAttack = (NumberPicker) findViewById(R.id.characterAttack);
        characterDefend = (NumberPicker) findViewById(R.id.characterDefend);

        String[] nums = new String[16];
        for(int i=0; i<nums.length; i++){nums[i] = Integer.toString(i);}

        //Body
        characterBody.setMinValue(0);
        characterBody.setMaxValue(15);
        characterBody.setWrapSelectorWheel(false);
        characterBody.setDisplayedValues(nums);
        characterBody.setValue(character.getBody());
        characterBody.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        //Mind
        characterMind.setMinValue(0);
        characterMind.setMaxValue(15);
        characterMind.setWrapSelectorWheel(false);
        characterMind.setDisplayedValues(nums);
        characterMind.setValue(character.getMind());
        characterMind.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        //Attack
        characterAttack.setMinValue(0);
        characterAttack.setMaxValue(15);
        characterAttack.setWrapSelectorWheel(false);
        characterAttack.setDisplayedValues(nums);
        characterAttack.setValue(character.getAttack());
        characterAttack.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        //Defend
        characterDefend.setMinValue(0);
        characterDefend.setMaxValue(15);
        characterDefend.setWrapSelectorWheel(false);
        characterDefend.setDisplayedValues(nums);
        characterDefend.setValue(character.getDefend());
        characterDefend.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        int image = getResources().getIdentifier(character.getCharacterClass().getClassName().toLowerCase(), "drawable", getPackageName());
        characterPortrait.setImageResource(image);

        characterName.setText(character.getCharacterClass().getClassName());
        characterDescription.setText(character.getCharacterClass().getClassDescription());




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_character, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_delete) {

            dataSource.open();
            dataSource.deleteCharacter(character);
            dataSource.close();

            super.onBackPressed();
            overridePendingTransition(R.transition.right_to_left_enter, R.transition.right_to_left_exit);
            return true;
        }

        if(id == android.R.id.home) {
            dataSource.open();
            character.setCharacterStats(characterMind.getValue(), characterBody.getValue(), characterAttack.getValue(),characterDefend.getValue(), character.getMove());
            dataSource.updateCharacter(character);
            dataSource.close();

            super.onBackPressed();
            overridePendingTransition(R.transition.right_to_left_enter, R.transition.right_to_left_exit);
        }

        if(id == R.id.action_write_nfc) {


            serializedCharacter = serializeCharacter(character);


            mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
            mNfcPendingIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, CharacterActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

            enableTagWriteMode();

            nfcDialog.show();
            nfcDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    disableTagWriteMode();
                }
            });

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        dataSource.open();
        character.setCharacterStats(characterMind.getValue(), characterBody.getValue(), characterAttack.getValue(),characterDefend.getValue(), character.getMove());
        dataSource.updateCharacter(character);
        dataSource.close();

        overridePendingTransition(R.transition.right_to_left_enter, R.transition.right_to_left_exit);
    }

    private void enableTagWriteMode() {
        mWriteMode = true;
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter[] mWriteTagFilters = new IntentFilter[] { tagDetected };
        mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mWriteTagFilters, null);
    }

    private void disableTagWriteMode() {
        mWriteMode = false;
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // Tag writing mode
        if (mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            NdefRecord record = NdefRecord.createMime( "application/character", serializedCharacter.getBytes());
            NdefMessage message = new NdefMessage(new NdefRecord[] { record });
            if (writeTag(message, detectedTag)) {
                Toast.makeText(this, "Success: Wrote character to nfc tag", Toast.LENGTH_LONG)
                        .show();
                nfcDialog.dismiss();
            }
        }
    }

    public boolean writeTag(NdefMessage message, Tag tag) {
        int size = message.toByteArray().length;
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Toast.makeText(getApplicationContext(),
                            "Error: tag not writable",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (ndef.getMaxSize() < size) {
                    Toast.makeText(getApplicationContext(),
                            "Error: tag too small",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                ndef.writeNdefMessage(message);
                return true;
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        return true;
                    } catch (IOException e) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    public String serializeCharacter(Character character) {

        String serializedString = "";

        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(character);
            so.flush();
            serializedString = bo.toString();
            serializedString = Base64.encodeToString(bo.toByteArray(), 0);

        } catch (Exception e) {
            System.out.println(e);
        }

        return serializedString;
    }





}
