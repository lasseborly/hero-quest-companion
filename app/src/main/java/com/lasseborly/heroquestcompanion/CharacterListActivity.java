package com.lasseborly.heroquestcompanion;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;


public class CharacterListActivity extends Activity {
    List<Character> characters = new ArrayList<Character>();
    public final static String CHARACTER = "character";

    HQCDataSource dataSource;
    ArrayAdapter<Character> characterArrayAdapter;
    ListView characterList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);



        dataSource = new HQCDataSource(this);
        dataSource.open();

        characters = dataSource.findAllCharacters();

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            NdefMessage[] messages = getNdefMessages(getIntent());
            byte[] payload = messages[0].getRecords()[0].getPayload();
            String characterString = new String(payload);

            try {
                byte[] characterBytes  = Base64.decode(characterString,0);
                ByteArrayInputStream bo = new ByteArrayInputStream(characterBytes);
                ObjectInputStream so = new ObjectInputStream(bo);
                Character c = (Character) so.readObject();
                characters.add(c);
                dataSource.createCharacter(c);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }


        characterArrayAdapter = new CharacterArrayAdapter(this, 0, characters);
        characterList = (ListView) findViewById(R.id.listView);
        characterList.setAdapter(characterArrayAdapter);

        characterList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Character character = characters.get(position);
                displayCharacter(character);

            }
        });

    }

    public void displayCharacter(Character character){
        Intent intent = new Intent(this, CharacterActivity.class);

        intent.putExtra(CHARACTER, character);

        startActivity(intent);
        overridePendingTransition(R.transition.left_to_right_enter, R.transition.left_to_right_exit);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_character_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new_character) {

            Intent intent = new Intent(this, CreateCharacterActivity.class);
            startActivity(intent);
            overridePendingTransition(R.transition.right_to_left_enter, R.transition.right_to_left_exit);
            
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        characters = dataSource.findAllCharacters();




        characterArrayAdapter = new CharacterArrayAdapter(this, 0, characters);
        characterList = (ListView) findViewById(R.id.listView);
        characterList.setAdapter(characterArrayAdapter);

        characterList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Character character = characters.get(position);
                displayCharacter(character);

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        //dataSource.close();
    }

    NdefMessage[] getNdefMessages(Intent intent) {
        // Parse the intent
        NdefMessage[] msgs = null;
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                // Unknown tag type
                byte[] empty = new byte[] {};
                NdefRecord record =
                        new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty);
                NdefMessage msg = new NdefMessage(new NdefRecord[] {
                        record
                });
                msgs = new NdefMessage[] {
                        msg
                };
            }
        } else {
            finish();
        }
        return msgs;
    }
}
