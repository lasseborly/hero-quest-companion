package com.lasseborly.heroquestcompanion;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ALILBO on 21-04-2015.
 */
public class Character implements Serializable {


    private static final long serialVersionUID = 2487333151936044555L;
    private long id;
    private String name;
    private CharacterClass characterClass;
    private int mind;
    private int body;
    private int attack;
    private int defend;
    private int move;
    private ArrayList<Equipment> equipments;
    private ArrayList<Task> tasks;



    public Character() {
    }

    public Character(String name, CharacterClass characterClass) {
        this.name = name;
        this.characterClass = characterClass;
    }

    public void setCharacterStats(int mind, int body, int attack, int defend, int move) {
        this.mind = mind;
        this.body = body;
        this.attack = attack;
        this.defend = defend;
        this.move = move;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + characterClass.toString();
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(CharacterClass characterClass) {
        this.characterClass = characterClass;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMind() {
        return mind;
    }

    public void setMind(int mind) {
        this.mind = mind;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public ArrayList<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(ArrayList<Equipment> equipments) {
        this.equipments = equipments;
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefend() {
        return defend;
    }

    public void setDefend(int defend) {
        this.defend = defend;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }
}
