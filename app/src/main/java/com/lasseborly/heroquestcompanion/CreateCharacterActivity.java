package com.lasseborly.heroquestcompanion;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;


public class CreateCharacterActivity extends Activity {

    TextView editCharacterName;
    HQCDataSource dataSource;
    RadioGroup editCharacterClass;
    CharacterClass characterClass = new CharacterClass();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_character);

        editCharacterName   = (TextView) findViewById(R.id.editCharacterName);

        editCharacterClass  = (RadioGroup) findViewById(R.id.editCharacterClass);

        dataSource = new HQCDataSource(this);
        dataSource.open();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_character, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_close) {
            super.onBackPressed();
            overridePendingTransition(R.transition.left_to_right_enter, R.transition.left_to_right_exit);

            return true;
        }
        else if (id == R.id.action_save) {

            Character character = new Character();

            character.setName(editCharacterName.getText().toString());

            Log.d("LOGTAG",  characterClass.toString());
            character.setCharacterClass(characterClass);
            character.setCharacterStats(characterClass.getClassMind(), characterClass.getClassBody(), characterClass.getClassAttack(), characterClass.getClassDefend(), characterClass.getClassMove());


            character = dataSource.createCharacter(character);

            dataSource.close();
            super.onBackPressed();
            overridePendingTransition(R.transition.left_to_right_enter, R.transition.left_to_right_exit);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.transition.left_to_right_enter, R.transition.left_to_right_exit);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioBarbarian:
                if (checked)
                    characterClass = dataSource.findCharacterClass(1);
                    break;
            case R.id.radioDwarf:
                if (checked)
                    characterClass = dataSource.findCharacterClass(2);
                    break;
            case R.id.radioElf:
                if (checked)
                    characterClass = dataSource.findCharacterClass(3);
                    break;
            case R.id.radioWizard:
                if (checked)
                    characterClass = dataSource.findCharacterClass(4);
                    break;
        }
    }


}
