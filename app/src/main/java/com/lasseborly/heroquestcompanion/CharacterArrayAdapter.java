package com.lasseborly.heroquestcompanion;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
/**
 * Created by ALILBO on 21-04-2015.
 */
public class CharacterArrayAdapter extends ArrayAdapter<Character> {

    private Context context;
    private List<Character> objects;

    public CharacterArrayAdapter(Context context, int resource, List<Character> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Character character = objects.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.character_item, null);

        TextView characterName = (TextView) view.findViewById(R.id.listCharacterName);
        characterName.setText(character.getName());
        TextView characterClass = (TextView) view.findViewById(R.id.listCharacterClass);
        characterClass.setText(character.getCharacterClass().getClassName());

        ImageView characterPortrait = (ImageView) view.findViewById(R.id.listCharacterPortrait);


        int image = context.getResources().getIdentifier(character.getCharacterClass().getClassName().toLowerCase(), "drawable", context.getPackageName());

        characterPortrait.setImageResource(image);
        return view;
    }
}
